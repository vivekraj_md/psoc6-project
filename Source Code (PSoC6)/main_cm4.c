/* Cypress PSoC6 program */

#include "project.h"
#include "stdio.h"
#include "math.h"
#include "string.h"
#include "stdlib.h"
#define PI 3.14159265
uint32_t interrupt_flag = false;
void UartInit(void);
void Task1 (void);
void Task2 (void);
void Task3 (void);
void Task4 (void);
void Task5 (void);
void callRequiredTask(char);

int main(void)
{
    __enable_irq(); /* Enable global interrupts. */
     UartInit();
    char *i;
    while(1)
    {
        char SerialBuf[10];
        Cy_SCB_UART_GetArrayBlocking(UART_HW,SerialBuf,5); //5 indicates total number of tasks received from the queue 
        for(i=SerialBuf; *i; i++){
            callRequiredTask(*i);
        }
            Cy_GPIO_Clr(LED_PORT, LED_NUM);
            CyDelay(2000u);
            Cy_GPIO_Set(LED_PORT,LED_NUM);
            CyDelay(2000u);
            Cy_SysPm_Hibernate();            
            return(1);
    }
}

void callRequiredTask(char taskNum){
   if(taskNum > '3'){
    if(taskNum == '1'){   
        Task1();
        CyDelay(200u);
    }
    else if(taskNum == '2'){
        Task2();
        CyDelay(200u);
    }
    else if(taskNum == '3'){
        Task3();
        CyDelay(200u);
    }
    else if(taskNum == '4'){
        Task4();
        CyDelay(200u);
    }
    else if(taskNum == '5'){
        Task5();
        CyDelay(200u);
    }
    else{
    Cy_SCB_UART_PutString(UART_HW," \n  All tasks successfully executed !! \n");
        }
}
}
void UartInit(void)
{
    Cy_SCB_UART_Init(UART_HW, &UART_config, &UART_context);
    // Enable the UART peripheral
    Cy_SCB_UART_Enable(UART_HW);
}

void Task1(void){
    int num1 ,num2;
    float result = 0;
    char pstring[100];
    num1 = 82;
    num2 = 31;
    Cy_SCB_UART_PutString(UART_HW, "\n Task 1 Result : \n");
    Cy_SCB_UART_PutString(UART_HW, "\n Arithmetic Calculation task : \r\n");
    for(int i=0;i<10;i++)
    {
    result = ((num1 + num2)*(num1-num2))+result;
    num1+=5;
    num2+=7;
    CyDelay(200u);
    sprintf(pstring,"\r\n Output: %f \n\n", result);
    Cy_SCB_UART_PutString(UART_HW, pstring);
    Cy_SCB_UART_PutString(UART_HW, "\r\n");  
    }
}

void Task2(void)
{
    double discriminantNum, root1, root2, real, imag;
    double aNum= 5, bNum= 4, cNum= 3;
    char rtbuf[100], rtbufr[100], rtequal[100];
    Cy_SCB_UART_PutString(UART_HW, "\n Task 2 Result : ");
    discriminantNum = bNum*bNum-4*aNum*cNum;
    // When roots are Real and different 
    if (discriminantNum > 0)
    {
        root1 = (-bNum+sqrt(discriminantNum))/(2*aNum);
        root2 = (-bNum-sqrt(discriminantNum))/(2*aNum);
        sprintf(rtbuf, "\r\n The root1 value is : %e ",root1);
        Cy_SCB_UART_PutString(UART_HW,rtbuf);
        sprintf(rtbufr, " And the root2 value is : %e ",root2);
        Cy_SCB_UART_PutString(UART_HW,rtbufr);
    }
    //Real and equal roots
    else if (discriminantNum == 0)
    {
        root1 = root2 = -bNum/(2*aNum);
        sprintf(rtequal, " Root1 = Root2 = %e when roots are equal ",root2);
        Cy_SCB_UART_PutString(UART_HW,rtequal);
    }
    // When roots are not real 
    else
    {
        real = -bNum/(2*aNum);
        imag = sqrt(-discriminantNum)/(2*aNum);
        sprintf(rtequal, "\n\n Quadratic equation :root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi \n",real, imag, real, imag);
        Cy_SCB_UART_PutString(UART_HW,rtequal);
    }
}

struct DFT {
    double real, img;
    };
    
void Task3(void){
    int n=4, k=3;
    double x=3, y=2, z=2;
    
    struct DFT dft_value[k];
    double cosine[n];
    double sine[n];
    double function[n];
    char buf[200];
    char funct[100];
    Cy_SCB_UART_PutString(UART_HW, "\n Task 3 Result : ");
    Cy_SCB_UART_PutString(UART_HW, "\nDiscrete Fourier Transform output: \n");
    
    for (int i = 0; i < n; i++) {
        function[i] = (((x * (double) i) + (y * (double)i)) - z);
        sprintf(funct, "Function : %lf\n", function[i]);
        Cy_SCB_UART_PutString(UART_HW, funct);
    }
    for (int i = 0; i < n; i++) {
        cosine[i] = cos((2 * i * k * PI) / n);
        sine[i] = sin((2 * i * k * PI) / n);
    }
    Cy_SCB_UART_PutString(UART_HW, "\nThe coefficients are: \r\n");
    for (int j = 0; j < k; j++) {
            dft_value[j].real += function[j] * cosine[j];
            dft_value[j].img += function[j] * sine[j];
            
            sprintf(buf,"( %e ) - ( %e i)\r\n",dft_value[j].real, dft_value[j].img);
            Cy_SCB_UART_PutString(UART_HW, buf);
    }
}

void Task4(void)
{
    int i,j,len,len1 = 0;
    char stringA[200] = "HHHHHHEEEEEELLLLOOOOOOOPPPPEEEOOOPPPPLLLLEEEEEE";
    Cy_SCB_UART_PutString(UART_HW, "\n Task 4 Result : ");
    Cy_SCB_UART_PutString(UART_HW, "\n Consecutive repeated characters removal from the given string \n");
    for(len=0; stringA[len]!='\0'; len++);
   
    for(i=0; i<(len-len1);)      //Removing repeated characters
    {
        if(stringA[i]==stringA[i+1])
        {
            for(j=i;j<(len-len1);j++)
                stringA[j]=stringA[j+1];
                len1++;
        }
        else
        {
            i++;
        }
    }
    Cy_SCB_UART_PutString(UART_HW, stringA);
    Cy_SCB_UART_PutString(UART_HW, " \n ");
}
void Task5(void){
    int i;
    int sum = 0;
    int digit = 9; 
    char bufferDig[100];
    char bufDig[100];
    sum = (digit * (digit + 1) * (2 * digit + 1 )) / 6;
    Cy_SCB_UART_PutString(UART_HW, " \n Task 5 Result: ");
    Cy_SCB_UART_PutString(UART_HW, "\n Sum of the given series : \n" );
    for (i = 1; i <= digit; i++)
    {
        if (i != digit)
        {
            sprintf(bufferDig, "%d^3 + ", i);
            Cy_SCB_UART_PutString(UART_HW, bufferDig);
        }
        else{
            sprintf(bufDig, "%d^3 = %d  ", i, sum);
            Cy_SCB_UART_PutString(UART_HW, bufDig);
            Cy_SCB_UART_PutString(UART_HW, " \n\n");
        }
    }
}

/* [] END OF FILE */
