#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <queue> 

#define _SCL_SECURE_NO_WARNINGS 
#pragma warning(disable:4996)
//#pragma warning(disable:4789) //WARNING: buffer of size N bytes will be overrun 

using namespace std;

class CypressTask {
public:
	int complexity;
	int tasknumber;
	
	bool operator < (const CypressTask& c) const {
		return (complexity < c.complexity ? true : false);
	}
	bool operator == (const CypressTask& c) const {
		return (complexity == c.complexity ? true : false);
	}

	CypressTask(int tasknumber, int complexity) {
		this->complexity = complexity;
		this->tasknumber = tasknumber;
	}
};

int main()
{
	system("python C:\\Users\\Vivekraj\\source\\repos\\CypressConnector\\lizard\\lizard-1.16.3\\lizard.py C:\\Users\\Vivekraj\\source\\repos\\CypressConnector\\lizard\\lizard-1.16.3\\main_cm0p.c");
	//Lizard to find the complexity of the code
	system("python C:\\Users\\Vivekraj\\source\\repos\\CypressConnector\\lizard\\lizard-1.16.3\\lizard.py C:\\Users\\Vivekraj\\source\\repos\\CypressConnector\\lizard\\lizard-1.16.3\\main_cm4.c");

	BOOL Write_Status;             // Status of the various operations 
	BOOL  Status;                 // Event mask to trigger  
	DCB dcbSerialParams = { 0 };
	HANDLE hCom;
	hCom = CreateFile(L"\\\\.\\COM8",                //port name
		GENERIC_READ | GENERIC_WRITE, //Read/Write
		0,                            // No Sharing
		NULL,                         // No Security
		OPEN_EXISTING,// Open existing port only
		0,            // Non Overlapped I/O
		NULL);        // Null for Comm Devices
	if (hCom == INVALID_HANDLE_VALUE)
		printf("Error in opening serial port\n");
	else
		printf("opening serial port successful");

	/*------------------------------- Setting the Parameters for the SerialPort ------------------------------*/

	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	Status = GetCommState(hCom, &dcbSerialParams);     //retreives  the current settings

	if (Status == FALSE)
		printf("\n   Error!  \n");

	dcbSerialParams.BaudRate = CBR_9600;      // Setting BaudRate = 9600
	dcbSerialParams.ByteSize = 8;             // Setting ByteSize = 8
	dcbSerialParams.StopBits = ONESTOPBIT;    // Setting StopBits = 1
	dcbSerialParams.Parity = NOPARITY;      // Setting Parity = None 
	Status = SetCommState(hCom, &dcbSerialParams);  //Configuring the port according to settings in DCB 

	if (Status == FALSE)
	{
		printf("\n   Error! in Setting DCB Structure");
	}
	else
	{
		printf("\n   Setting DCB Structure Successfull\n");
		printf("\n       Baudrate = %d", dcbSerialParams.BaudRate);
		printf("\n       ByteSize = %d", dcbSerialParams.ByteSize);
		printf("\n       StopBits = %d", dcbSerialParams.StopBits);
		printf("\n       Parity   = %d", dcbSerialParams.Parity);
	}
	
	/*---------------------------------------------Writing serial port data---------------------------------------*/
	priority_queue <CypressTask> gqueue; //Queue of tasks
	CypressTask t1 = CypressTask(1, 2);
	CypressTask t2 = CypressTask(2, 3);
	CypressTask t3 = CypressTask(3, 4);
	CypressTask t4 = CypressTask(4, 5);
	CypressTask t5 = CypressTask(5, 3);
	gqueue.push(t1);
	gqueue.push(t2);
	gqueue.push(t3);
	gqueue.push(t4);
	gqueue.push(t5);

	char tknumber[500];
	int  dNoOFBytestoWrite;               // No of bytes to write into the port
	DWORD  dNoOfBytesWritten;             // No of bytes written to the port
	char lpBuffer[500];

	for (int i = 0;i <5;i++)
	{
		if (gqueue.empty()) {
			printf("Queue is empty");
		}
		else {
			CypressTask task = gqueue.top();
			task.tasknumber;
			//printf("Task output %d\n", task.tasknumber);
			sprintf(tknumber, "%d", task.tasknumber);
			
			*lpBuffer = *tknumber;		 // lpBuffer should be  char

			dNoOFBytestoWrite = sizeof(*lpBuffer);		// Calculating the no of bytes to write into the port
			Write_Status = WriteFile(hCom,               // Handle to the Serialport
				lpBuffer,							 // Data to be written to the port 
				dNoOFBytestoWrite,					 // No of bytes to write into the port
				&dNoOfBytesWritten,					 // No of bytes written to the port
				NULL);
			gqueue.pop();
		}
	}
	printf("\n\n   Task has be pushed to the queue succesfully \r\n");
	
	if (Write_Status == TRUE)

		//printf("Number of bytes %c \n", dNoOfBytesWritten);
		printf("\n\n   Data written to  port Successfully \n");
	else
		printf("\n\n   Error %d in Writing to Serial Port \n");

	/*---------------------------------------------Reading serial port data---------------------------------------*/
	
	char TempChar = 0; //Temporary character used for reading
	char SerialBuffer[25600];//Buffer for storing Rxed Data
	DWORD NoBytesRead;
	int i = 0;

	if (Status == FALSE)
	{
		printf("\n    Error! in Setting WaitCommEvent()");
	}
	else //If  WaitCommEvent()==True Read the RXed data using ReadFile();
	{
		printf("\n\n   Reading UART serial port is completed ...  \n");
		do
		{
			ReadFile(hCom,           //Handle of the Serial port
				&TempChar,       //Temporary character
				sizeof(TempChar),//Size of TempChar
				&NoBytesRead,    //Number of bytes read
				NULL);
			SerialBuffer[i++] = TempChar;// Store Tempchar into buffer
		//	printf(" %s", SerialBuffer);
		} while (NoBytesRead > 0);

		printf("\n\n    ");
		int j = 0;
		for (j = 0; j < i - 1; j++)
		{                                // j < i-1 to remove the dupliated last character
			printf("%c", SerialBuffer[j]);
		}
		SerialBuffer[j] = 0;
		SerialBuffer[i] = 0;
		TempChar = 0;
	}
	CloseHandle(hCom);//Closing the Serial Port
	return 0;
}
